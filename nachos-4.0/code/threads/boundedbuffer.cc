// boundedbuffer.cc
//  This is a skeleton for the implementation of bounded buffer to test
//  synchronization primitives (condition and lock).
//
//  Your implementation of bounded buffer should be thread safe. For an
//  example of implementation refer to synchlist implementation.
//
// Created by Andrzej Bednarski

#include "boundedbuffer.h"
#include "synch.h"
#include<iostream>
using namespace std;
//--
// The constructor creates a bounded buffer of a given size
//--
BoundedBuffer::BoundedBuffer(int maxsize)
{
	capacity = maxsize;
	mutex = new Semaphore("MUTEX", 1);
	empty = new Semaphore("EMPTY", 0);
  	full = new Semaphore("FULL", 0);
	
	itemCount = 0;
 	in = 0;
 	out = 0;
	rsize = 0;
	wsize = 0;
 	buffer = new char[capacity];
 	
  	
}

//-- 
// Clean up the memory (deallocate members you have allocated basically in
// the constructor).
//--
BoundedBuffer::~BoundedBuffer()
{
	delete buffer;
  	delete mutex;
  	delete empty;
 	delete full;
}
//--
// Write an integer (item) to the queue of the bounded buffer. Weak up a thread
// waiting on an element to be read.
//--
static int isWL = 0;
static int isRL = 0;
int BoundedBuffer::Write(void* data, int size)
{
	cout<<"W"<<endl;
	wsize=size;	
	if(wsize>capacity){Close(); return 0;}
	do{
		if(isWL&&isRL){Close(); return 0;}
		if(capacity-itemCount<wsize){
			isWL=1;
			empty->P();
		}
		mutex->P();
		for(int i=0;i<wsize;i++){
		buffer[in] = ((char*)data)[i];
		in = (in + 1) % capacity;
		itemCount++;
		}
		mutex->V();
		if(isRL&&(capacity-itemCount>=rsize) ){				
			isRL = 0;			
			full->V();
		}
	}while(true);
	return size;
}

//--
// Read an integer (item) from the head of the bounded buffer. Weak up a thread
// waiting on an element to be written.
//--
int BoundedBuffer::Read(void* data, int size)
{
	cout<<"R"<<endl;
	rsize=size;
	if(rsize>capacity){Close(); return 0;}
	do{
		if(isWL&&isRL){Close(); return 0;}
		if(itemCount<rsize){
			isRL=1;
			full->P();
		}
		mutex->P();
		for(int i=0;i<size;i++){
			((char*)data)[i]=buffer[out];
			out = (out + 1) % capacity;
			itemCount--;
		}
		mutex->V();
		if(isWL&&(itemCount>=wsize) ){
			isWL =0;
			empty->V();
		}
		
	}while(true);
	return size;
}
void BoundedBuffer::Close()
{
	delete this;
}

int BoundedBuffer::mWrite(Temp which)
{
    return Write(which.data, which.size);
}
static BoundedBuffer bb(10);

static void testThead(Temp tm){
	bb.mWrite(tm);
}

void BoundedBuffer::SelfTest()
{
    //bb = new BoundedBuffer(4); 
    Thread *t = new Thread("forked thread");
    Temp tm;
    int* cc;
    int *aa ;
    *aa=20;
    tm.data = (void*)aa;
    tm.size = 4;
    t->Fork((VoidFunctionPtr)testThead,(void *)&tm );
    bb.Read((void*)cc, 4);
    cout<<*cc;
}
