#include "list.h"
#include "synch.h"
struct Temp{
	void* data;
	int size;
};
class BoundedBuffer{
private:
    char* buffer;
    int in, out;
    int wsize, rsize;
    int capacity;      // capacity of the bounded buffer.
    int itemCount;     // total items in the bounded buffer
    int inCS;          // count of items in critical section (should be 0 or 1)
    Semaphore *mutex;
    Semaphore *empty;  // wait in Remove if the list is empty
    Semaphore *full;   // wait in Append if the list is FULL.
  

public:
	BoundedBuffer(int maxsize);
	~BoundedBuffer();
	int Read(void*, int);
	int Write(void*, int);
	int mWrite(Temp which);
	void Close();
	void SelfTest();
};
