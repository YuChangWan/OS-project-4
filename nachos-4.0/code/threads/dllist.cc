#include "dllist.h"
#include "iostream"
using namespace std;
DLLElement::DLLElement(void *itemPtr, int sortKey) {
	next = NULL;
	prev = NULL;
	key = sortKey;
	item = itemPtr;
}

DLList::DLList() {
	first = NULL;
	last = NULL;
}
DLList::~DLList() {
	int i;
	while (last) {
		this->Remove(&i);
	}
}
void DLList::Prepend(void *item) {	//add to head of list (set key = min_key-1) 
	DLLElement *new_Element;
	if (IsEmpty()) {
		new_Element = new DLLElement(item, first->key - 1);
		new_Element->next = first;
		first->prev = new_Element;
		first = new_Element;
	}
	else {
		new_Element = new DLLElement(item, 0);
		first = new_Element;
		last = new_Element;
	}
}
void DLList::Append(void *item) { // add to tail of list (set key = max_key+1) 
	DLLElement *new_Element;
	if (IsEmpty()) {
		new_Element = new DLLElement(item, last->key + 1);
		new_Element->prev = last;
		last->next = new_Element;
		last = new_Element;
	}
	else {
		new_Element = new DLLElement(item, 0);
		first = new_Element;
		last = new_Element;
	}
}
void *DLList::Remove(int *keyPtr) {  // remove from head of list
	DLLElement *temp_Element;
	void * temp_item;
	if (IsEmpty()) {
		*keyPtr = first->key;
		temp_item = first->item;
		if (first != last) {
			temp_Element = first->next;//
			delete first;
			first = temp_Element;
		}
		else {
			delete first;
			first = last = NULL;
		}
		
	}
	else {
		temp_item = keyPtr;
	}
	return temp_item;
}
bool DLList::IsEmpty() {             // return true if list has elements
	bool ret_value = true;
	if (first == NULL)ret_value = false;
	return ret_value;
}
void DLList::SortedInsert(void *item, int sortKey) {
	DLLElement *iter_Element, *temp_Element;
	temp_Element = new DLLElement(item, sortKey);
	if (IsEmpty()) {
		for (iter_Element = first; iter_Element != NULL && sortKey >= iter_Element->key; iter_Element = iter_Element->next);
		if (iter_Element == NULL) {
			last->next = temp_Element;
			temp_Element->prev = last;
			last = temp_Element;
		}
		else {
			if (iter_Element != first) {
				iter_Element->prev->next = temp_Element;
				temp_Element->prev = iter_Element->prev;
			}
			else {
				first = temp_Element;
			}
			iter_Element->prev = temp_Element;
			temp_Element->next = iter_Element;
		}
	}
	else {
		first = temp_Element;
		last = temp_Element;
	}
}
void *DLList::SortedRemove(int sortKey) {  // remove first item with key==sortKey
	DLLElement *iter_Element;
	void * temp_item;
	if (IsEmpty()) {
		for (iter_Element = first; iter_Element != NULL && sortKey > iter_Element->key; iter_Element = iter_Element->next);
		temp_item = iter_Element->item;
		if (iter_Element != first) {
			iter_Element->prev->next = iter_Element->next;
		}
		else {
			first = iter_Element->next;
		}
		if (iter_Element != last) {
			iter_Element->next->prev = iter_Element->prev;
		}
		else {
			last = iter_Element->prev;
		}
		delete iter_Element;
	}
	else {
		temp_item = NULL;
	}
	return temp_item;
}

